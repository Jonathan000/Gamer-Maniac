name: DOTA 2
description: It is an Online multiplayer game. In the game there are 2 teams Radiant and Dire,
 each team has 5 players and 11 towers and 1 Ancient(Power Base) to protect.
 The team which destroys the enemy Ancient first is the winner.
This is just the overview but the game has 100+ heroes and many items which makes the competitive interaction more Interesting.
video link: https://www.youtube.com/watch?v=5MuZmHEGqXQ
img link: https://www.google.co.id/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwjOxtXeqsPcAhUHp48KHf4RDiwQjRx6BAgBEAU&url=https%3A%2F%2Fwww.twitch.tv%2Fdirectory%2Fgame%2FDota%25202&psig=AOvVaw0TuQv4P9rWlZFUGv1Fx4lU&ust=1532920157087046

name: Arena Of Valor (AOV)
description: is a multiplayer online battle arena developed and published by Tencent Games. By 2017,
 it was reported that the game had over 80 million daily active players and 200 million monthly active players,
 and was both the world's highest grossing game as well as the most downloaded app globally.
 video link:https://www.youtube.com/watch?v=Ac-3S0Yzapw
 img link:https://www.google.co.id/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwj58Oj-q8PcAhWKuY8KHQ2rDqgQjRx6BAgBEAU&url=http%3A%2F%2Fbali.tribunnews.com%2F2018%2F05%2F06%2Fgame-arena-of-valor-jadi-cabang-olahraga-di-asian-games-2018&psig=AOvVaw232Rd2dl2FffPzEKAgaF_S&ust=1532920481576774

 name: League of Legends (LoL)
 description:League of Legends is a fast-paced, competitive online game that blends the speed and intensity of an RTS with RPG elements.
 Two teams of powerful champions, each with a unique design and playstyle,
 battle head-to-head across multiple battlefields and game modes.
 With an ever-expanding roster of champions, frequent updates and a thriving tournament scene,
 League of Legends offers endless replayability for players of every skill level.
 video link:https://www.youtube.com/watch?v=BGtROJeMPeE
 img link:https://www.google.co.id/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwjdxv6CrsPcAhWIK48KHWOpBS4QjRx6BAgBEAU&url=https%3A%2F%2Fwww.makeuseof.com%2Ftag%2Fwhat-is-league-of-legends%2F&psig=AOvVaw3zS1l_nddIEObUx_9uJmR9&ust=1532921041376461

name: Mobile Legends Bang Bang (MLBB)
description: Mobile Legends Bang Bang is a multiplayer online battle arena (MOBA) game designed for mobile phones.
The two opposing teams fight to reach and destroy the enemy's base while defending their own base for control of a path,
the three "lanes" known as "top", "middle" and "bottom", which connects the bases.
In each team, there are five players who each control an avatar, known as a "hero", from their own device.
Weaker computer-controlled characters, called "minions",
spawn at team bases and follow the three lanes to the opposite team's base, fighting enemies and turrets.
video link:https://www.youtube.com/watch?v=AnBdw7JmF94
img link:https://www.google.co.id/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwjGh6Opr8PcAhXFMY8KHbKfDOQQjRx6BAgBEAU&url=https%3A%2F%2Fwww.trentech.id%2Fcerita-hero-mobile-legends%2F&psig=AOvVaw0UP_W3oUiKdoV2H2w5bQcR&ust=1532921385464854

name: Vainglory
description: is a video game developed and published by Super Evil Megacorp for iOS and Android devices.
The game is a version of the genre where in two opposing teams of three or five players fight to destroy the enemy base,
by controlling the path between the bases, which is lined by turrets and guarded by AI-controlled enemy creatures.
Off the path, players battle for control points that provide resources.
The game was released for iOS in November 2014, after being soft-launched for over half a year,
with the Android version being released in July 2015.
video link:https://5v5.vainglorygame.com/
img lnk:https://www.google.co.id/url?sa=i&source=images&cd=&cad=rja&uact=8&ved=2ahUKEwiQgKPIsMPcAhWHMI8KHXgTDeIQjRx6BAgBEAU&url=https%3A%2F%2Fwww.vainglorygame.com%2Fnews%2Fupdate-2-3-notes-charms-passes-streamlined-experiences%2F&psig=AOvVaw1Du82zCp3Frh1rhUgZOYEE&ust=1532921722257810
